<?php
declare(strict_types=1);

namespace App\Application\Actions\Factura;

use Psr\Http\Message\ResponseInterface as Response;

class ViewFacturaAction extends FacturaAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $facturaId = (int) $this->resolveArg('id');
        $factura = $this->viewFacturaServiceInterface->viewFactura($facturaId);  
       $this->logger->info("Factura con id `${facturaId}` se ha recuperado.");

        return $this->respondWithData($factura);
    }
}
