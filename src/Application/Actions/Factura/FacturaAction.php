<?php
declare(strict_types=1);

namespace App\Application\Actions\Factura;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use App\Interfaces\Factura\AddFacturaServiceInterface;
use App\Interfaces\Factura\ListFacturaServiceInterface;
use App\Interfaces\Factura\ViewFacturaServiceInterface;
abstract class FacturaAction extends Action
{
    /**
     * @var AddFacturaServiceInterface
     */
    protected $addFacturaServiceInterface;
    
    /**
     * @var ListFacturaServiceInterface
     */
    protected $listFacturaServiceInterface;
      /**
     * @var ViewFacturaServiceInterface
     */
    protected $viewFacturaServiceInterface;
    /**
     * @param LoggerInterface $logger
     * @param AddFacturaServiceInterface  $addFacturaServiceInterface
     */
    public function __construct(LoggerInterface $logger, AddFacturaServiceInterface $addFacturaServiceInterface, ListFacturaServiceInterface $listFacturaServiceInterface, ViewFacturaServiceInterface $viewFacturaServiceInterface)
    {
   
       
        parent::__construct($logger);
        $this->addFacturaServiceInterface = $addFacturaServiceInterface;
        $this->listFacturaServiceInterface = $listFacturaServiceInterface;
        $this->viewFacturaServiceInterface = $viewFacturaServiceInterface;
    }
}
