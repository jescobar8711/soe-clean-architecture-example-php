<?php
declare(strict_types=1);

namespace App\Application\Actions\User;
use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use App\Interfaces\User\AddUserServiceInterface;

abstract class UserAction extends Action
{
    /**
     * @var AddUserServiceInterface
     */
    protected $addUserServiceInterface;

    /**
     * @param LoggerInterface $logger
     * @param UserAddServiceInterface  $userAddServiceInterface
     */
    public function __construct(LoggerInterface $logger, AddUserServiceInterface $addUserServiceInterface)
    {
      
        parent::__construct($logger);
        $this->addUserServiceInterface=$addUserServiceInterface;
    }
}
