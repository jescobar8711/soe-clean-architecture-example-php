<?php
declare(strict_types=1);
namespace App\Application\Actions\User;
use Psr\Http\Message\ResponseInterface as Response;

class AddUserAction extends UserAction
{
    
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {

       $body = $this->request->getParsedBody();
      
        $userId = $this->addUserServiceInterface->createUser($body);

        $this->logger->info("Usuario id: `${userId}` Insertado.");

        return $this->respondWithData($userId);
    }
}
