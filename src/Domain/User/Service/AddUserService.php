<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\User\Service;
   
use App\Domain\User\Entity\User;
use  App\Domain\User\ValueObjects\Login;
use App\Domain\User\Repository\AddUserRepository;
use App\Interfaces\User\AddUserServiceInterface;
use Selective\Validation\Exception\ValidationException;

/**
 * Domain Service.
 */
 class AddUserService implements AddUserServiceInterface
{
     
    /**
     * @var UserRepository
     */
    private $repository;

   

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(
       
        AddUserRepository $repository
    ) {
       
        $this->repository = $repository;
    }

    /**
     * Create a new user.
     *
     * @param array $user The user data
     *
     * @throws ValidationException
     *
     * @return int The new user ID
     */
    public function createUser(Array $user): int
    {
        $login =   new Login($user['login']);
        $psw = (string)  $user['psw'];
        $email = (string)  $user['email'];
        $userEntity= new User($userId=null,$login,$psw,$email);
        $userId = $this->repository->insertUser($userEntity);
        return $userId;
    }
}