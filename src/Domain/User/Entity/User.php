<?php
declare(strict_types=1);

namespace App\Domain\User\Entity;

use  App\Domain\User\ValueObjects\Login;
use JsonSerializable;

  class User implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;


    /**
     * @var Login
     */
    private $login;

    /**
     * @var string
     */
    private $psw;

    /**
     * @var string
     */
    private $mail;
 
    /**
     * @param int|null  $id
     * @param Login    $login
     * @param string    $psw
     * @param string    $mail
     */
    public function __construct(?int $id, Login $login, string $psw, string $mail)
    {
        $this->id = $id;
        $this->login = $login;
        $this->psw = $psw;
        $this->mail = $mail;
    }

    public function getId():?int {
        return $this->id;
    }

    public function getLogin(): Login {
        return $this->login;
    }

    public function getPsw() : string{
        return $this->psw;
    }

    public function getMail() : string{
        return $this->mail;
    }

        /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'psw' => $this->psw,
            'email' => $this->mail,
        ];
    }
}