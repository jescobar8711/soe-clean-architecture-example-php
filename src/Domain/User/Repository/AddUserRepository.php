<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\User\Repository;

use App\Domain\User\Entity\User;
use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;

/**
 * Repository.
 */
class AddUserRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;

    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        
        $this->queryFactory = $queryFactory;
      
    }

    /**
     * Insert user row.
     *
     * @param UserCreatorData $user The user
     *
     * @return int The new ID
     */
    public function insertUser(User $user): int
    {
        $row = [
            'login' => $user->getLogin()->getValue(),
            'psw' => $user->getPsw(),
            'mail' => $user->getMail(),
        ];
        return (int)$this->queryFactory->newInsert(TableName::USER, $row)->execute()->lastInsertId();
    }
}
