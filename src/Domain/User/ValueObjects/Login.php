<?php

namespace  App\Domain\User\ValueObjects;


final class Login
{
    /**
     * @var string
     */
    private $login;

    /**
     * Username constructor.
     *
     * @param string $login
     */
    public function __construct(string $login)
    {
      $max=15;
        if (empty($login)) {
            throw new \InvalidArgumentException("Name can't be empty");
        }
        if (strlen($login) >  $max) {
            throw new \InvalidArgumentException("Name '$login' must be less than '$max' chars");
        }
        $this->login = $login;
    }

    /**
     * Return the name from the value object
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->login;
    }
}