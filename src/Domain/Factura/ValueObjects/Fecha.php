<?php

namespace  App\Domain\Factura\ValueObjects;


final class Fecha
{
    /**
     * @var string
     */
    private $fecha;

    /**
     * Nrofactura constructor.
     *
     * @param int $fecha
     */
    public function __construct(string $fecha)
    {
        if (empty($fecha)) {
            throw new \InvalidArgumentException("fecha no puede estar vacio");
        }
        $this->fecha = $fecha;
    }

    /**
     * Return the name from the value object
     *
     * @return Date
     */
    public function getValue(): string
    {
        return $this->fecha;
    }
}