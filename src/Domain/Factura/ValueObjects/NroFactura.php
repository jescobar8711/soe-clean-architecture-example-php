<?php

namespace  App\Domain\Factura\ValueObjects;


final class Nrofactura
{
    /**
     * @var int
     */
    private $nrofactura;

    /**
     * Nrofactura constructor.
     *
     * @param int $nrofactura
     */
    public function __construct(int $nrofactura)
    {
      $min=0;
      $max=200;
        if (empty($nrofactura)) {
            throw new \InvalidArgumentException("Nro Factura can't be empty");
        }
        if ($min < $nrofactura &&  $nrofactura>=  $max) {
            throw new \InvalidArgumentException("Name '$nrofactura' debe ser mayor que '$min' y menor que '$max' ");
        }
        $this->nrofactura = $nrofactura;
    }

    /**
     * Return the name from the value object
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->nrofactura;
    }
}