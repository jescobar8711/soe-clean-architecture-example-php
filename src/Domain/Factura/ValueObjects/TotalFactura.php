<?php

namespace  App\Domain\Factura\ValueObjects;


final class TotalFactura
{
    /**
     * @var float
     */
    private $totalFactura;

    /**
     * Nrofactura constructor.
     *
     * @param int $totalFactura
     */
    public function __construct(float $totalFactura)
    {
        if (empty($totalFactura)) {
            throw new \InvalidArgumentException("total factura no puede estar vacio");
        }
        $this->totalFactura = $totalFactura;
    }

    /**
     * Return the name from the value object
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->totalFactura;
    }
}