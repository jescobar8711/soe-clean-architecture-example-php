<?php
declare(strict_types=1);

namespace App\Domain\Factura\Entity;

use App\Domain\Factura\ValueObjects\Nrofactura;
use App\Domain\Factura\ValueObjects\Fecha;
use App\Domain\Factura\ValueObjects\TotalFactura;
use App\Domain\Factura\ValueObjects\Cliente;
use JsonSerializable;
class Factura implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;


    /**
     * @var Nrofactura
     */
    private $nro;

    /**
     * @var Fecha
     */
    private $fecha;

    /**
     * @var TotalFactura
     */
    private $total;
 /**
     * @var Cliente
     */
    private $cliente;
    /**
     * @param int|null  $id
     * @param Nrofactura    $nrofactura
     * @param date    $fecha
     * * @param float    $total
     * @param int    $cliente
     */
    public function __construct(?int $id, Nrofactura $nro, Fecha $fecha, TotalFactura $total, Cliente $cliente)
    {
        $this->id = $id;
        $this->nro = $nro;
        $this->fecha = $fecha;
        $this->total = $total;
        $this->cliente =$cliente;
    }
    
    public function getId() :?int{
        return $this->id;
    }

   public  function getNro(): Nrofactura {
        return $this->nro;
    }

    public function getFecha(): Fecha{
        return $this->fecha;
    }

    public function getTotal(): TotalFactura {
        return $this->total;
    }

    public function getCliente():Cliente {
        return $this->cliente;
    }


 

        /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'nro' => $this->nrofactura,
            'fecha' => $this->fecha,
            'total' => $this->total,
            'cliente' => $this->cleinte,
        ];
    }
}