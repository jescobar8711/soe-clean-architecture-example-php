<?php
declare(strict_types=1);

namespace App\Domain\Factura\Entity;

use JsonSerializable;
class FacturaDetalle implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;


    /**
     * @var int
     */
    private $factura;

    /**
     * @var int
     */
    private $producto;
       /**
     * @var int
     */
    private $cantidad;
    /**
     * @var float
     */
    private $precio;
  /**
     * @var float
     */
    private $total;
   /**
     * @param int|null  $id
     * @param int $factura
     * @param int $producto
     * * @param int $cantidad
     * @param float $total
     */
    public function __construct(?int $id, int $factura, int $producto, int $cantidad,float $precio, float $total) {
        $this->id = $id;
        $this->factura = $factura;
        $this->producto = $producto;
        $this->cantidad = $cantidad;
         $this->precio = $precio;
        $this->total = $total;
    }
  
    public function getId():?int {
        return $this->id;
    }

    public function getFactura():int {
        return $this->factura;
    }

    public function getProducto():int {
        return $this->producto;
    }

    public function getCantidad():int {
        return $this->cantidad;
    }
  public function getPrecio(): float {
        return $this->total;
    }
    public function getTotal(): float {
        return $this->total;
    }

    
    

        /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'factura' => $this->factura,
            'producto' => $this->producto,
            'cantidad' => $this->cantidad,
            'precio' => $this->precio,
            'total' => $this->total,
        ];
    }
}