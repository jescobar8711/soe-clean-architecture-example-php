<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Repository;


use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;
/**
 * Repository.
 */
class ViewFacturaRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;
   
    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        
        $this->queryFactory = $queryFactory;
      
    }
    
     /**
     * Load data table entries.
     *
     * @param array $params The factura
     *
     * @return array The table data
     */
    public function ViewFactura(int $id): array
    {
    $query = $this->queryFactory->newSelect(TableName::FACTURA)->select('*')
            ->andWhere(array("id"=>$id));
        $rows = $query->execute()->fetch("assoc");
        return $rows;
    }
        
}
