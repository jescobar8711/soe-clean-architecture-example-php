<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Repository;

use App\Domain\Factura\Entity\Factura;
use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;

/**
 * Repository.
 */
class AddFacturaRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;

    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        
        $this->queryFactory = $queryFactory;
      
    }

    /**
     * Insert factura row.
     *
     * @param UserCreatorData $factura The user
     *
     * @return int The new ID
     */
    public function insertFactura(Factura $factura): int
    {
          $row = [
          //  'nro' => $factura->getNro()->getValue(),
            'fecha' => $factura->getFecha()->getValue(),
            'total' => $factura->getTotal()->getValue(),
            'cliente_id' => $factura->getCliente()->getValue(), 
        ];
        return (int)$this->queryFactory->newInsert(TableName::FACTURA, $row)->execute()->lastInsertId();
    }
}
