<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Repository;


use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;
use Cake\Database\StatementInterface;
/**
 * Repository.
 */
class ListFacturaRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;
   
    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        
        $this->queryFactory = $queryFactory;
      
    }
    
     /**
     * Load data table entries.
     *
     * @param array $params The factura
     *
     * @return array The table data
     */
    public function getTableData(array $params): array
    {
    $query = $this->queryFactory->newSelect(TableName::FACTURA)->select('*');

        $rows = $query->execute()->fetchAll(StatementInterface::FETCH_TYPE_ASSOC);
        return $rows;
    }
        
}
