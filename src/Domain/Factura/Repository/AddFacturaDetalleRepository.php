<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Repository;

use App\Domain\Factura\Entity\FacturaDetalle;
use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;

/**
 * Repository.
 */
class AddFacturaDetalleRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;

    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        
        $this->queryFactory = $queryFactory;
      
    }

    /**
     * Insert factura row.
     *
     * @param UserCreatorData $facturaDetalle The user
     *
     * @return int The new ID
     */
    public function insertFacturaDetalle(FacturaDetalle $facturaDetalle): int
    {
          $row = [
            'factura_id' => $facturaDetalle->getFactura(),
            'producto_id' => $facturaDetalle->getProducto(),
            'cantidad' => $facturaDetalle->getCantidad(),
            'precio' => $facturaDetalle->getPrecio(),
            'total' => $facturaDetalle->getTotal(),
            
        ];
        return (int)$this->queryFactory->newInsert(TableName::FACTURA_DETALLE, $row)->execute()->lastInsertId();
    }
}
