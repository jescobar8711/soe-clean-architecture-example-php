<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Service;
   
use App\Domain\Factura\Entity\Factura;
use App\Domain\Factura\Entity\FacturaDetalle;
use App\Domain\Factura\ValueObjects\Nrofactura;
use App\Domain\Factura\ValueObjects\Fecha;
use App\Domain\Factura\ValueObjects\TotalFactura;
use App\Domain\Factura\ValueObjects\Cliente;
use App\Domain\Factura\Repository\AddFacturaRepository;
use App\Domain\Factura\Repository\AddFacturaDetalleRepository;
use App\Interfaces\Factura\AddFacturaServiceInterface;
use Selective\Validation\Exception\ValidationException;

/**
 * Domain Service.
 */
  class AddFacturaService implements AddFacturaServiceInterface
{
     
    /**
     * @var FacturaRepository
     */
    private $facturaRepository;
  
    /**
     * @var FacturaDetalleRepository
     */
    private $facturaDetalleRepository;
   

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(
       
        AddFacturaRepository $facturaRepository,AddFacturaDetalleRepository $facturaDetalleRepository
    ) {
       
        $this->facturaRepository = $facturaRepository;
        $this->facturaDetalleRepository = $facturaDetalleRepository;
    }

    /**
     * Create a new factura.
     *
     * @param array $factura The user data
     *
     * @throws ValidationException
     *
     * @return int The new factruta ID and detalle de ids
     */
    public function createFactura(Array $factura): array
    {
       $nro =   new Nrofactura((int)1);
       $fecha = new Fecha(\date("Y/m/d"));
        $total = new TotalFactura(floatval($factura['total']));
        $cliente = new Cliente((int)$factura['cliente']);
        $detalle = $factura['detalle'];
        $facturaEntity=new Factura((int)$id=null, $nro, $fecha, $total, $cliente);
        $facturaId = $this->facturaRepository->insertFactura($facturaEntity);
        
        foreach ($detalle as $key => $value) {
            $facturaDetalleEntity=new FacturaDetalle((int)$id=null, (int)$facturaId, (int)$value['producto'], (int)$value['cantidad'], (float)$value['precio'], (float)$value['total']);
            $facturaDetalleId = $this->facturaDetalleRepository->insertFacturaDetalle($facturaDetalleEntity);
            $facturadetalleIds[]=$facturaDetalleId;
               
        }
        
        return array("id"=>$facturaId,"detalle"=>$facturadetalleIds);
    }
}