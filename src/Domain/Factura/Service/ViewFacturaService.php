<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Service;
   

use App\Domain\Factura\Repository\ViewFacturaRepository;
use App\Domain\Factura\Repository\ListFacturaDetalleRepository;
use App\Interfaces\Factura\ViewFacturaServiceInterface;
use Selective\Validation\Exception\ValidationException;

/**
 * Domain Service.
 */
  class ViewFacturaService implements ViewFacturaServiceInterface
{
     
    /**
     * @var FacturaRepository
     */
    private $facturaRepository;
  
    /**
     * @var FacturaDetalleRepository
     */
    private $facturaDetalleRepository;
   

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(
       
        ViewFacturaRepository $facturaRepository,ListFacturaDetalleRepository $facturaDetalleRepository
    ) {
       
        $this->facturaRepository = $facturaRepository;
        $this->facturaDetalleRepository = $facturaDetalleRepository;
    }

    /**
     * Create a new factura.
     *
     * @param array $factura The user data
     *
     * @throws ValidationException
     *
     * @return int The new factruta ID and detalle de ids
     */
    public function viewFactura(int $id): array
    {
        $factura = (array)$this->facturaRepository->viewFactura($id);
        $factura["detalle"]=(array)$this->facturaDetalleRepository->getTableData(array("factura_id"=>$id));
        return $factura;
    }
}