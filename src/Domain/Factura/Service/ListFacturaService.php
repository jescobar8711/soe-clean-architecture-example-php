<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Domain\Factura\Service;

use App\Domain\Factura\Repository\ListFacturaRepository;
use App\Interfaces\Factura\ListFacturaServiceInterface;
use Selective\Validation\Exception\ValidationException;

/**
 * Domain Service.
 */
  class ListFacturaService implements ListFacturaServiceInterface
{
     
    /**
     * @var ListFacturaRepository
     */
    private $facturaRepository;
  
   

    /**
     * The constructor.
     *
     * @param FacturaRepository $repository The repository
     */
    public function __construct(
       
    ListFacturaRepository $facturaRepository
    ) {
       
        $this->facturaRepository = $facturaRepository;
    }

    /**
     * Create a new factura.
     *
     * @param array $factura The user data
     *
     * @throws ValidationException
     *
     * @return int The List facturas e ids
     */
    public function ListFactura(array $params): array
    {
   
        $facturas = $this->facturaRepository->getTableData($params);
        
        
        return $facturas;
    }
}