<?php
declare(strict_types=1);
use App\Domain\User\Service\AddUserService;
use App\Interfaces\User\AddUserServiceInterface;
use App\Domain\Factura\Service\AddFacturaService;
use App\Interfaces\Factura\AddFacturaServiceInterface;
use App\Domain\Factura\Service\ListFacturaService;
use App\Interfaces\Factura\ListFacturaServiceInterface;
use App\Domain\Factura\Service\ViewFacturaService;
use App\Interfaces\Factura\ViewFacturaServiceInterface;
use DI\ContainerBuilder;
 
return function (ContainerBuilder $containerBuilder) {
    // Here we map our service interface to its in memory implementation
    $containerBuilder->addDefinitions([
        AddUserServiceInterface::class => \DI\autowire(AddUserService::class),
        AddFacturaServiceInterface::class => \DI\autowire(AddFacturaService::class),
        ListFacturaServiceInterface::class => \DI\autowire(ListFacturaService::class),
        ViewFacturaServiceInterface::class => \DI\autowire(ViewFacturaService::class),
     ]);

};
