<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Cake\Database\Connection;
use Selective\BasePath\BasePathMiddleware;
use Selective\Config\Configuration;
use Selective\Validation\Encoder\JsonEncoder;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
    ]);
            $containerBuilder->addDefinitions([
            Connection ::class=> function (ContainerInterface $c) {
            $settings = $c->get('settings');
            $dbSettings = $settings['db'];
             return new Connection($dbSettings);
        },
    ]); 
            $containerBuilder->addDefinitions([
            PDO::class => function (ContainerInterface $c) { 
            $db = $c->get(Connection::class);
            $driver = $db->getDriver();
            $driver->connect();
            return $driver->getConnection();
            },
    ]);
     
        
};
