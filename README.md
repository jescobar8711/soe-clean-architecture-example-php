# soe-clean-architecture-example

aplicacion ejemplo de clean-architecture-example en PHP ejecicio practico de SOE-MDEIS-MOD06
la aplicasion usa Slim 4 con implementacion  Slim PSR-7  y implemetado en contenedor  PHP-DI. 

Aplicación construida para Composer.

## Install the Application

Ejecuta este comando desde el directorio en el que se encuentra instalar tu nueva aplicación
clonar la aplicacion
```bash
git clone git@gitlab.com:jescobar8711/soe-clean-architecture-example.git
```
Para ejecutar la aplicación en desarrollo, se pueden ejecutar estos comandos 
```bash
cd soe-clean-architecture-example
composer install
composer start
```

o se puede usar  `docker-compose` y ejecutar tu aplicacion con `docker`, puedes ejecutar con estos comandos:
```bash
cd soe-clean-architecture-example
docker-compose up -d
```
Después de eso, abre `http://localhost:8080` en tu navegador.

aplicacion utiliza database postgres
actualmente esta conectada a servicio PostgreSQL en  server motty.db.elephantsql.com




